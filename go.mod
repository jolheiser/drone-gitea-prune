module go.jolheiser.com/drone-gitea-prune

go 1.14

require (
	code.gitea.io/sdk/gitea v0.12.1
	github.com/urfave/cli/v2 v2.2.0
	go.jolheiser.com/beaver v1.0.2
)
