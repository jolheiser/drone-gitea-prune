# Drone Gitea Release Prune

## Settings

Required
* `base` - Base Gitea URL
* `token` - Gitea API token

Optional
* `owner` - Repository owner (default: `DRONE_REPO_OWNER`)
* `repo` - Repository name (default: `DRONE_REPO_NAME`)
* `debug` - Debug mode
* `dry` - Don't actually prune
* `keep` - Number of releases to keep (default: 5)
* `max` - Maximum number of releases to check (default: 0 -> all of them)