package main

import (
	"os"

	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

var Version = "develop"

func main() {
	app := cli.NewApp()
	app.Name = "Drone Gitea Release Prune"
	app.Description = "Drone plugin to prune Gitea release attachments"
	app.Version = Version
	app.Action = doAction

	app.Flags = []cli.Flag{
		&cli.BoolFlag{
			Name:    "debug",
			Usage:   "Turn on debug mode",
			EnvVars: []string{"PLUGIN_DEBUG"},
		},
		&cli.BoolFlag{
			Name:    "dry",
			Usage:   "Dry run",
			EnvVars: []string{"PLUGIN_DRY"},
		},
		&cli.StringFlag{
			Name:    "base",
			Usage:   "Gitea base URL",
			EnvVars: []string{"PLUGIN_BASE"},
			Value:   "https://gitea.com",
		},
		&cli.StringFlag{
			Name:    "token",
			Usage:   "Gitea API token",
			EnvVars: []string{"PLUGIN_TOKEN"},
		},
		&cli.StringFlag{
			Name:    "owner",
			Usage:   "Gitea owner",
			EnvVars: []string{"DRONE_REPO_OWNER", "PLUGIN_OWNER"},
		},
		&cli.StringFlag{
			Name:    "repo",
			Usage:   "Gitea repo",
			EnvVars: []string{"DRONE_REPO_NAME", "PLUGIN_REPO"},
		},
		&cli.IntFlag{
			Name:    "keep",
			Usage:   "Keep x releases",
			EnvVars: []string{"PLUGIN_KEEP"},
			Value:   5,
		},
		&cli.IntFlag{
			Name:    "max",
			Usage:   "Max amount of releases to check",
			EnvVars: []string{"PLUGIN_MAX"},
			Value:   0,
		},
	}

	if err := app.Run(os.Args); err != nil {
		beaver.Fatal(err)
	}

}
