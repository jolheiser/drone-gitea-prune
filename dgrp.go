package main

import (
	"os"

	"code.gitea.io/sdk/gitea"
	"github.com/urfave/cli/v2"
	"go.jolheiser.com/beaver"
)

func doAction(ctx *cli.Context) error {
	beaver.Console.Format = beaver.FormatOptions{
		LevelPrefix: true,
	}

	if ctx.Bool("debug") {
		beaver.Console.Level = beaver.DEBUG
	}

	if ctx.Bool("dry") {
		beaver.Info("///// DRY RUN /////")
	}

	sanity(ctx)

	client := gitea.NewClient(ctx.String("base"), ctx.String("token"))

	releases, err := calcReleases(client, ctx.String("owner"), ctx.String("repo"), ctx.Int("max"))
	if err != nil {
		return err
	}

	for idx, r := range releases {
		if idx < ctx.Int("keep") {
			continue
		}

		for _, a := range r.Attachments {
			beaver.Debugf("Deleting attachment for release %s (#%d): %s", r.TagName, r.ID, a.Name)
			if !ctx.Bool("dry") {
				if err := client.DeleteReleaseAttachment(ctx.String("owner"), ctx.String("repo"), r.ID, a.ID); err != nil {
					return err
				}
			}
		}
	}

	return nil
}

func sanity(ctx *cli.Context) {
	exit := false
	if !ctx.IsSet("token") {
		exit = true
		beaver.Error("--token is required")
	}
	if !ctx.IsSet("owner") {
		exit = true
		beaver.Error("--owner is required")
	}
	if !ctx.IsSet("repo") {
		exit = true
		beaver.Error("--repo is required")
	}
	if exit {
		os.Exit(1)
	}
}

func calcReleases(client *gitea.Client, owner, repo string, max int) ([]*gitea.Release, error) {
	releases := make([]*gitea.Release, 0)
	page := 1

loopReleases:
	for {
		opts := gitea.ListReleasesOptions{
			ListOptions: gitea.ListOptions{
				Page:     page,
				PageSize: 50, // Gitea max
			},
		}
		rel, err := client.ListReleases(owner, repo, opts)
		if err != nil {
			return releases, err
		}
		beaver.Debugf("Gitea returned %d releases for page %d", len(rel), page)

		for _, r := range rel {
			if len(r.Attachments) == 0 {
				break
			}
			releases = append(releases, r)

			if max > 0 && len(releases) == max {
				break loopReleases
			}
		}

		if len(rel) < opts.PageSize {
			break
		}
	}

	return releases, nil
}
